# Issues

Sitata's issue tracker for user-generated bug reports.

Please [submit a "new issue"](https://gitlab.com/sitata-public/issues/-/issues/new) when you discover a bug or some unexpected behaviour from our software.

## Documentation

Sitata's API documentation and supporting guides can be [accessed here](https://docs.sitata.com).


